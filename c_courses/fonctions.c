#include "fonctions.h"

/*
 * Calcule la moyenne entre 2 entiers.
 * Retourne le résultat sous forme de réel
 */
double moyenne (int x, int y) {
    return (double) (x+y)/2;
}

/*
 * Recherche du maximum dans un tableau
 * Retourne la position du max
 */
int positionMaxTab (int *t, unsigned int size, bool (*ordre)(int a, int b)) {
	int positionMax = 0;

	for (int i = 1; i < size; i++)
		if (ordre(t[positionMax], t[i]))
			positionMax = i;
	return positionMax;
}

/*
 * Recherche du minimum dans un tableau
 * Retourne la position du max
 */
int positionMinTab (int *t, unsigned int size, bool (*ordre)(int a, int b)) {
	int positionMin = 0;

	for (int i = 1; i < size; i++) {
		if (!ordre(t[positionMin], t[i]))
			positionMin = i;
	}
	return positionMin;
}

/*
 * Échange 2 entiers dans un tableau d'entier
 */
void echanger (int *t, int p1, int p2) {
	int tmp = t[p1];
	t[p1] = t[p2];
	t[p2] = tmp;
}

/*
 * Échange 2 entiers dans un tableau d'entier
 */
void echanger2 (int *p1, int *p2) {
	int tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}

/*
 * Échange 2 entiers dans un tableau d'entier
 */
void echanger3 (unsigned int *p1, unsigned int *p2) {
	unsigned int tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}

/*
 * Trie par un tableau d'entier par la recherche du maximum
 */
void triSelectionMax (int *t, unsigned int size, bool (*ordre)(int a, int b)) {
	if (size == 1)
		return;
	int positionMax = positionMaxTab(t, size, ordre);
	echanger(t, positionMax, size-1);
	triSelectionMax(t, size-1, ordre);
}

/*
 * Trie par un tableau d'entier par la recherche du maximum
 */
void triSelectionMin (int *t, unsigned int size, bool (*ordre)(int a, int b)) {
	if (size == 1)
		return;
	int positionMin = positionMinTab(t, size, ordre);
	echanger2(t+positionMin, t);
	triSelectionMin(t+1, size-1, ordre);
}

/*
 * Affiche un tableau d'entier
 */
void afficheTableauEntier (int *t, unsigned int *size) {
	for (int i = 0; i < *size; i++)
		printf("%d\n", t[i]);
}

/*
 * Alloue un tableau d'entier
 * Retourne le tableau alloué
 */
int * initTableauEntier (unsigned int *size) {
	int * entier;
	if ((entier = (int *) malloc(*size * sizeof(int))) == NULL) {
		fprintf(stderr, "Allocation impossible !\n");
		exit(EXIT_FAILURE);
	}
	return entier;
}

/*
 * Ré-alloue un tableau d'entier
 * Retourne le tableau alloué
 */
int * allocationTableauEntierDynamique (int *t, unsigned int *size) {
	int * tmp;
	if ((tmp = (int *)realloc(t, *size * sizeof(int))) == NULL) {
		fprintf(stderr, "Allocation impossible !\n");
		exit(EXIT_FAILURE);
	}
	return tmp;
}

/*
 * Rempli un tableau d'entier à partir de saisie utilisateur
 */
void remplirTableauEntier (int *t, unsigned int *size) {
	int tmp = 0;
	int i = 0;
	while (scanf("%d", &tmp) == 1) {
		if (i >= *size) {
			*size += *size;
			t = allocationTableauEntierDynamique(t, size);
		}
		t[i++] = tmp;
		printf("*** t[%d]: %d\n", i-1, t[i-1]);
	}
	if (i != *size) {
		*size = i;
		t = allocationTableauEntierDynamique(t, size);
	}
}

/*
 * Rempli un tableau d'entier à partir de saisie utilisateur
 * Retourne le tableau rempli
 */
int* lireEntier(unsigned int *taille) {
	int *t;
	t = initTableauEntier(taille);
	int i = 0;
	int tmp = 0;
	while (scanf("%d", &tmp) == 1) {
		if (i >= *taille) {
			*taille += *taille;
			t = allocationTableauEntierDynamique(t, taille);
		}
		t[i++] = tmp;
	}
	if (i != *taille) {
		*taille = i;
		t = allocationTableauEntierDynamique(t, taille);
	}

	return t;
}

/*
 * Recherche l'entier maximum dans un tableau d'entier
 * Retourne l'entier maximum
 */
int maxInt (int *t, unsigned int size) {
	int max = 0;
	for (int i = 0; i < size; i++)
		if (max < t[i]) max = t[i];
	return max;
}

/*
 * Trie un tableau d'entier par la méthode de tri par comptage
 */
void triParComptage (int *t, unsigned int size) {
	int max = maxInt(t, size);
	int *count = (int *) calloc(max, sizeof(int));
	int pos = 0;

	for (int i = 0; i < size; i++)
		count[t[i]]++;
	for (int i = 0; i < max; i ++)
		while (count[i]-- != 0)
			t[pos++] = i;
}

/*
 * Retourne le booléen résultant de l'oopération logique
 */
bool inc (int a, int b) {
	return a < b;
}

/*
 * Retourne le booléen résultant de l'oopération logique
 */
bool dec (int a, int b) {
	return a > b;
}

/*
 * Recherche dichotomique d'1 entier dans un tableu d'entier
 */
unsigned int rechercheDicho (int *t, unsigned int taille, int val) {
	// MODE ITÉRATIF
	/*if (taille <= 1)
		return taille;
	int milieu = 0;
	int ind = 0;
	if ((taille % 2) == 0)
		milieu = taille/2-1;
	else
		milieu = (taille-1)/2;
	printf("***m: %d; t: %d\n", milieu, taille);
	printf("***T[%d]: %d\n", milieu, t[milieu]);
	if (t[milieu] == val) {
		return milieu;
	}
	else if (t[milieu] > val) {
		ind = rechercheDicho(t, milieu, val);
		printf("***if - ind: %d == %d ? %d : %d\n", ind, milieu-1, ind+milieu+1, ind+1);
		return (ind == milieu-1) ? ind+milieu+1 : ind+1;
	}
	else {
		ind = rechercheDicho(t+milieu+1, taille-milieu-1, val);
		printf("***else - ind: %d == %d ? %d : %d\n", ind, taille+ind, taille+ind+1, ind+milieu);
		return (ind == taille+ind) ? taille+ind+1 : ind+milieu;
	}
//	return (ind == -1) ? taille : ind;
	*/
	// MODE RÉCURSIF
	int deb = 0, fin = taille-1;
	while (deb <= fin) {
		printf("***d: %d; f: %d\n", deb, fin);
		unsigned int milieu = (fin-deb)/2;
		printf("***T[%d]=%d\n", milieu, t[milieu]);
		if (t[milieu] == val)
			return milieu;
		if (t[milieu] > val)
			fin = milieu-1;
		else deb = milieu+1;
	}
	return taille;
}
