#include "io.h"

/*
 * Lecture d'un fichier dont le nom est passé en paramètre
 * Retourne une chaîne de caractère contenant 
 */
char * readFile (char* fichier) {
	char *chaine;
	FILE *f;
	size_t taille;

	if ((f = fopen(fichier, "r")) == NULL) {
		fprintf(stderr, "Can not read \"%s\"!\n", fichier);
		exit(EXIT_FAILURE);
	}
	// On récupère le nombre de bloc à lire pour lire le fichier d'un seul coup
	fseek(f, 0, SEEK_END);
	taille = ftell(f);
	fseek(f, 0, SEEK_SET);
	// Allocation de l'espace mémoire nécesaire pour stocker le contenu du fichier
	if ((chaine = (char *) malloc((taille+1) * sizeof(char))) == NULL) {
		fprintf(stderr, "Can not allocate memory!\n");
		exit(EXIT_FAILURE);
	}
	// Lecture du fichier
	if (fread(chaine, taille, 1, f) != 1) {
		fprintf(stderr, "Can not read \"%s\"!\n", fichier);
		exit(EXIT_FAILURE);
	}
	chaine[taille] = '\0';
	fclose(f);
	return chaine;
}

/*
 * Ré-alloue une chaîne de caractère
 */
char * reallocateChar (char *c, size_t size) {
	char *tmp;
	if ((tmp = (char *) realloc(c, size*sizeof(char))) == NULL) {
		fprintf(stderr, "Can not allocate memory!\n");
		exit(EXIT_FAILURE);
	}
	return tmp;
}

/*
 * Récupère une séquence d'ADN à partir d'une chaîne de caractère
 */
size_t getSeqFromFasta(char *c) {
	int posToKeep = 0, i = 0;
	while(c[posToKeep++] != '\n' && c[posToKeep] != '\0');
	while (c[posToKeep] != '\0') {
		if (isBase(c[posToKeep])) c[i++] = c[posToKeep];
		posToKeep++;
	}
	c[i] = '\0';
	c = reallocateChar(c, i++);
	return (size_t) i;
}

/*
 * Cherche le début d'une séquence d'ADN
 * Retourne la position de la séquence
 */
size_t prepareSequenceString (char *text) {
	int nbSeq = 0, posToKeep = 0, readPos = 0;

	while (text[posToKeep] != '\0') {
		while(text[posToKeep] != '\n' && text[posToKeep] != '\0') posToKeep++;
		while(text[posToKeep] != '\0' && text[posToKeep] != '>') {
			if (isBase(text[posToKeep])) text[readPos++] = text[posToKeep];
			posToKeep++;
		}
		text[readPos++] = '\0';
		nbSeq++;
	}
	reallocateChar(text, readPos);
	return (size_t) readPos;
}

/*
 * Range les séquences d'ADN dans une liste
 */
Seq * getMultiFasta(char *text, size_t readPos, size_t sizeText) {
	if (readPos >= sizeText)
		return NULL;

	Seq *new = allocSeq(1);
	new->seq = text+readPos;
	new->size = strlen(new->seq)+1;
	new->seqRC = reverseDNA(new->seq);
	new->gcRatio = GCRatio(new->seq);
	readPos = readPos + new->size;
	new->next = getMultiFasta(text, readPos, sizeText);
	return new;
}

/*
 * Alloue une liste de séquence
 */
Seq * allocSeq (size_t size) {
	Seq * s;
	if ((s = (Seq *) malloc(size * sizeof(Seq))) == NULL) {
		fprintf(stderr, "Can't allocate \"Seq\"!\n");
		exit(EXIT_FAILURE);
	}
	return s;
}
