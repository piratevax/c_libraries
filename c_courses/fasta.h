#ifndef __FASTA__
#define __FASTA__

	#include "global.h"
	#include "fonctions.h"

	typedef struct _Seq {
		char *seq;
		char *seqRC;
		size_t size;
		double gcRatio;
		struct _Seq *next;
	}Seq;

	typedef struct _Plot {
		int **plot;
		size_t col;
		size_t line;
	}Plot;

	bool isBase (char c);
	char complementDNA (char c);
	char* reverseDNA (char *seq);
	double GCRatio (char *seq);
	void printSeq (Seq *allSeq);
	void printSortedSeq (Seq *allSeq, size_t size, unsigned int *sortedPos);
	unsigned int * triParLongueur (Seq *allSeq, unsigned int size, bool (*ordre)(int a, int b));
	void freeSeq(Seq *s);
	Seq * delSeq (Seq *s, bool (*fieldToTest)(Seq*));
	bool gcOverFifty (Seq *s);
	Plot * allocPlot (size_t line, size_t col);
	void alloc2DIntTab(Plot *dotplot);
	void freeDotplot (Plot *dotplot);
	void printDotplot (Plot *dotplot, char *col, char* line);
	Plot * getDotplot (char *col, size_t sizeC, char *line, size_t sizeL);
	void longestSharedSeq(Plot *dotplot, char *seq);

#endif
