#include "main.h"

int main (int argc, char **argv) {
	char *f, *text;
	Seq *allSeq = NULL;
	Plot **dotplot = NULL;
	if (argc <= 1) {
		fprintf(stderr, "No input arguments!\nUsage:\n\tDotPlot <file(s).fa>\n");
		return EXIT_FAILURE;
	}
	int arg = argc;
	// Le programme s'exécute tant qu'il y a des arguments
	while (arg > 1) {
		f = argv[--arg];
		text = readFile(f);
		printf("\n> %s\n\n", f);
		size_t textSize =  prepareSequenceString (text);
		allSeq = getMultiFasta(text, 0, textSize);
		printSeq(allSeq);
		if ((dotplot = (Plot **)malloc(PLOT*sizeof(Plot *))) == NULL) {
			fprintf(stderr, "Can't allocate memory!\n");
			return EXIT_FAILURE;
		}
		dotplot[0] = getDotplot(allSeq->seq, allSeq->size-1, allSeq->next->seq, allSeq->next->size-1);
		dotplot[1] = getDotplot(allSeq->seq, allSeq->size-1, allSeq->next->seqRC, allSeq->next->size-1);
		dotplot[2] = getDotplot(allSeq->seqRC, allSeq->size-1, allSeq->next->seq, allSeq->next->size-1);
		dotplot[3] = getDotplot(allSeq->seqRC, allSeq->size-1, allSeq->next->seqRC, allSeq->next->size-1);
			//printDotplot(dotplot[i], allSeq->seq, allSeq->next->seq);
		for (int i = 0; i < PLOT; i++) {
			longestSharedSeq(dotplot[i], allSeq->seq);
			freeDotplot(dotplot[i]);
		}
		//freeSeq(allSeq);
		free(dotplot);
	}

	return EXIT_SUCCESS;
}
