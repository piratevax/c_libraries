#ifndef __FONCTIONS__
#define __FONCTIONS__
#include "global.h"

    double moyenne (int x, int y);
	int positionMaxTab (int *t, unsigned int size, bool (*ordre)(int a, int b));
	void echanger (int *t, int p1, int p2);
	void echanger2 (int *p1, int *p2);
	void echanger3 (unsigned int *p1, unsigned int *p2);
	void triSelectionMax (int *t, unsigned int size, bool (*ordre)(int a, int b));
	void afficheTableauEntier (int *t, unsigned int *size);
	int * initTableauEntier (unsigned int *size);
	int * allocationTableauEntierDynamique (int *t, unsigned int *size);
	void remplirTableauEntier (int *t, unsigned int *size);
	int* lireEntier(unsigned int *taille);
	int maxInt (int *t, unsigned int size);
	void triParComptage (int *t, unsigned int size);
	int positionMinTab (int *t, unsigned int size, bool (*ordre)(int a, int b));
	void triSelectionMin (int *t, unsigned int size, bool (*ordre)(int a, int b));
	bool inc (int a, int b);
	bool dec (int a, int b);
	unsigned int rechercheDicho (int *t, unsigned int taille, int val);

#endif
