#ifndef __IO__
#define __IO__

#include "global.h"
#include "fasta.h"

	char * readFile (char* file);
	char * reallocateChar (char *c, size_t size);
	size_t getSeqFromFasta(char *c);
	size_t prepareSequenceString (char *text);
	Seq * getMultiFasta(char *text, size_t readPos, size_t sizeText);
	Seq * allocSeq (size_t size);

#endif
