#include "fasta.h"

/*
 * Vérifie si le caractère est une base d'ADN
 * Retourne un booléen
 */
bool isBase (char c) {
	switch (toupper(c)) {
		case 'A': return true;
		case 'T': return true;
		case 'C': return true;
		case 'G': return true;
		case 'N': return true;
		default: return false;
	}
}

/*
 * Retourne la base complémentaire au caractère
 */
char complementDNA (char c) {
	switch (toupper(c)) {
		case 'A': return 'T';
		case 'T': return 'A';
		case 'C': return 'G';
		case 'G': return 'C';
		default: return 'N';
	}
}

/*
 * Inverse l'ordre des caractères dans une chaîne de caractères
 * Retourne la chaîne de caractères inversée
 */
char* reverseDNA (char* seq) {
	int i, j, size = strlen(seq);
	char * reverse = (char *) malloc(size * sizeof(char));
	
	for (i = size-1, j = 0; i > -1; i--, j++)
		reverse[i] = complementDNA(seq[j]);
	reverse[size] = '\0';
	return reverse;
}

/*
 * Calcule le raio GC d'une séquence d'ADN contenu dans une chaîne de caractères
 * Retourne le ratio au type double
 */
double GCRatio (char *seq) {
	int i, ratio = 0;

	for (i = 0; seq[i] != '\0'; i++)
		if (seq[i] == 'C' || seq[i] == 'G')
			ratio++;
	return (double) (ratio) / (i-1);
}

/*
 * Affichage récursif des séquences présentes dans la liste allSeq
 */
void printSeq (Seq *allSeq) {
	if (allSeq == NULL)
		return;
	printf("SEQ: %s\nSEQ_RC: %s\nRATIO_GC: %lf\nSIZE: %ld\n", allSeq->seq, allSeq->seqRC, allSeq->gcRatio, allSeq->size);
	printSeq(allSeq->next);
}

/*
 * Affichage d'une séquences a une position précise
 */
void printSortedSeq (Seq *allSeq, size_t size, unsigned int *sortedPos) {
	for (int i = 0; i < size; i++)
		printf("SEQ: %s\nRATIO_GC: %lf\nSIZE: %ld\n", allSeq[sortedPos[i]].seq, allSeq[sortedPos[i]].gcRatio, allSeq[sortedPos[i]].size);
}

/*
 * Tri des séquences par longeur
 * Retourne un tableau d'entier contenant 
 */
unsigned int * triParLongueur (Seq *allSeq, unsigned int size, bool (*ordre)(int a, int b)) {
	unsigned int *sortedPos = NULL, seqSize[size];

	if ((sortedPos = (unsigned int*) malloc(size * sizeof(unsigned int))) == NULL) {
		fprintf(stderr,"Can't allocate \"unsigned int\"!\n");
		exit(EXIT_FAILURE);
	}
	for (int i = 0; i < size; i++) {
		seqSize[i] = allSeq[i].size;
		sortedPos[i] = i;
	}
	for (int i = size; i > 1; i--) {
		for (int j = 0; j < i-1; j++) {
			if (!ordre(seqSize[j], seqSize[j+1])){
				echanger3(&seqSize[j], &seqSize[j+1]);
				echanger3(&sortedPos[j], &sortedPos[j+1]);
			}
		}
	}
	return sortedPos;
}

/*
 * Libération des espaces mémoires alloués pour les séquences
 */
void freeSeq(Seq *s) {
	if (s->next != NULL)
		freeSeq(s->next);
	free(s->seq);
	free(s->seqRC);
	free(s);
}

/*
 * Suppression d'une séquence
 */
Seq * delSeq (Seq *s, bool (*fieldToTest)(Seq*)) {
	// MODE RÉCURSIF
	if (fieldToTest(s)) {
		if (s->next == NULL)
			return NULL;
		Seq *tmp = s->next;
		free(s);
		return delSeq(tmp, fieldToTest);
	}
	if (s->next == NULL)
		return NULL;
	s->next = delSeq(s->next, fieldToTest);
	return s;
	// MODE ITÉRATIF
	/*Seq *first = NULL;
	Seq *prec = NULL;
	while (s != NULL) {
		if (fieldToTest(s)) {
			if (prec == NULL){
				Seq* tmp = s;
				s = s->next;
				first = s;
				free(tmp);
			}
			else {
				Seq *tmp = s;
				prec->next = s->next;
				s = s->next;
				free(tmp);
			}
		}
		else {
			if (prec == NULL)
				first = s;
			prec = s;
			s = s->next;
		}
	}
	return first;*/
}

/*
 * Teste si le ratio GC est supérieur à 0.5
 * Retourne un booléen
 */
bool gcOverFifty (Seq *s) {
	return s->gcRatio > .5;
}

/*
 * Alloue l'espace en mémoire pour la structure
 * Retourne la structure allouée
 */
Plot * allocPlot (size_t line, size_t col) {
	Plot * dotplot = NULL;
	if ((dotplot = (Plot *)malloc(sizeof(Plot))) == NULL) {
		fprintf(stderr, "Can't allocate memory!\n");
		exit(EXIT_FAILURE);
	}
	dotplot->col = col;
	dotplot->line = line;
	alloc2DIntTab(dotplot);
	return dotplot;
}

/*
 * Alloue l'espace en mémoire pour le tableau qui contiendra la repréentation du dotplot sous forme d'entier
 */
void alloc2DIntTab(Plot *dotplot) {
	if ((dotplot->plot = (int **)malloc(dotplot->col * sizeof(int *))) == NULL) {
		fprintf(stderr, "Can't allocate memory!\n");
		exit(EXIT_FAILURE);
	}
	for (int i = 0; i < dotplot->col; i++) {
		if ((dotplot->plot[i] = (int *) malloc(dotplot->line * sizeof(int))) == NULL) {
			fprintf(stderr, "Can't allocate memory!\n");
			exit(EXIT_FAILURE);
		}
	}
}

/*
 * Libère l'espace mémoire d'une strucutre Plot
 */
void freeDotplot (Plot *dotplot) {
	for (int i = 0; i < dotplot->col; i++)
		free(dotplot->plot[i]);
	free(dotplot->plot);
	free(dotplot);
}

/*
 * Génère le dotplot pour 2 séquences
 * Retourne le dotplot dans une structure
 */
Plot * getDotplot (char *col, size_t sizeC, char *line, size_t sizeL) {
	Plot *dotplot = allocPlot(sizeL, sizeC);
	for (int i = 0; i < sizeC; i++)
		for (int j = 0; j < sizeL; j++)
			// Teste si la base est identique dans les 2 chaînes de caractères lorqu'il s'agit de la première colonne et de la première ligne
			if (col[i] == line[j] && (i == 0 || j == 0))
				dotplot->plot[i][j] = 1;
			// Teste si la base est identique dans les 2 chaînes de caractère
			// on ajoute un à la valeur présente dasn la diagonale supérieure ainsi exemple :
			/*     GCAGGT
			 *   T 000001
			 *   C 010000
			 *   A 002000
			 *   G 200310
			 *   G 100140
			 *   G 100120
			 */
			else if (col[i] == line[j])
				dotplot->plot[i][j] = 1 + dotplot->plot[i-1][j-1];
			else
				dotplot->plot[i][j] = 0;
	return dotplot;
}

/*
 * Affiche le dotplot
 */
void printDotplot (Plot *dotplot, char *col, char* line) {
#ifdef DEBUG
	printf("*** c: %ld; l: %ld\n", dotplot->col, dotplot->line);
#endif
	printf("  %s\n", col);
	for (size_t i = 0; i < dotplot->line; i++) {
		for (size_t j = 0; j < dotplot->col; j++) {
			if (j == 0) printf("%c ", line[i]);
#ifndef DEBUG
			(dotplot->plot[j][i] != 1) ? printf("*") : printf("_");
#endif
#ifdef DEBUG
			printf("%d", dotplot->plot[j][i]);
#endif
		}
		printf("\n");
	}
}

/*
 * Détermine la plus longue sous-chaîne identique à partir d'un dotplot
 */
void longestSharedSeq(Plot *dotplot, char *seq) {
	int xMax = 0, yMax = 0, max = 0;

	for (int i = 0; i < dotplot->col; i++) {
		for (int j = 0; j < dotplot->line; j++) {
			if (dotplot->plot[i][j] > max) {
				max = dotplot->plot[i][j];
				xMax = i;
				yMax = j;
			}
		}
	}
#ifdef DEBUG
	printf("*** max: %d; x: %d; y: %d\n", max, xMax, yMax);
#endif
	printf("LONGEST SHARED SEQ: %d\n\t\tx: %d,%d\n\t\ty: %d,%d\n", max, xMax, xMax+max-1, yMax, yMax+max-1);
	char subSeq[max+1];
	for (int i = max-1; i > -1; i--)
		subSeq[i] = seq[xMax--];
	subSeq[max] = '\0';
	printf("\t%s\n", subSeq);
}
