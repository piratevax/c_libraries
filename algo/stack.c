#include "stack.h"

List emptyStack() {
	return emptyList();
}

int isEmptyStack (List l) {
	return isEmptyList(l);
}

int getTop(List l) {
	return *((int*) getElement(l));
}

List push (List l, int* element) {
	return addAtTheBegining(l, element);
}

List pop (List l) {
	List tmp = l;
	l = getNext(l);
	free(tmp);
	return l;
}
