#include "tree.h"

Tree emptyTree() {
	return NULL;
}

int isEmptyTree(Tree t) {
	return t == NULL;
}

Element root(Tree t) {
	return t->element;
}

Tree left(Tree t) {
	return t->left;
}

Tree right(Tree t) {
	return t->right;
}

Tree build(Element element, Tree left, Tree right) {
	Tree t = NULL;
	
	if ((t = (Tree) malloc(sizeof(struct _Tree))) == NULL) {
		fprintf(stderr, "Can't allocate memory!\n");
		exit(1);
	}
	t->element = element;
	t->left = left;
	t->right = right;
	return t;
}

void prefixProcess (Tree t) {
	if (!isEmptyTree(t)) {
		printf("%d, ", root(t));
		prefixProcess(left(t));
		prefixProcess(right(t));
	}
}

void infixProcess(Tree t) {
	if (!isEmptyTree(t)) {
		infixProcess(left(t));
		printf("%d, ", root(t));
		infixProcess(right(t));
	}
}

void suffixProcess(Tree t) {
	if (!isEmptyTree(t)) {
		suffixProcess(left(t));
		suffixProcess(right(t));
		printf("%d, ", root(t));
	}
}

void process(Tree t, int mode) {
	switch(mode) {
		case 1:
			prefixProcess(t);
			printf("\n");
			break;
		case 2:
			infixProcess(t);
			printf("\n");
			break;
		case 3:
			suffixProcess(t);
			printf("\n");
			break;
		default:
			fprintf(stderr,"Unknown process mode!\n");
			break;
	}
}

void printTree(Tree t, int level) {
	if (isEmptyTree(t))
		return;
	for (int i = 0; i <= level; i++)
		printf("|");
	printf("- %d\n", root(t));
	printTree(left(t), level+1);
	printTree(right(t), level+1);
}

Tree insert(Element element, Tree t) {
	if (isEmptyTree(t))
		return build(element, emptyTree(), emptyTree());
	if (root(t) < element)
		t->right = insert(element, right(t));
	else
		t->left = insert(element, left(t));
	return t;
}

int search(Element element, Tree t) {
	if (isEmptyTree(t))
		return 0;
	if (root(t) == element)
		return 1;
	if (root(t) < element)
		return search(element, right(t));
	return search(element, left(t));
}

Element max(Tree t) {
	if (isEmptyTree(right(t)))
		return root(t);
	return max(right(t));
}

Tree delete(Element element, Tree t) {
	if (root(t) == element) {
		int l = isEmptyTree(left(t)), r = isEmptyTree(right(t));
		if (l && r) {
			free(t);
			return emptyTree();
		}
		Tree tmp;
		if (!l && r) {
			tmp = left(t);
			free(t);
			return tmp;
		}
		tmp = right(t);
		free(t);
		return tmp;
	}
	if (root(t) < element)
		t->right = delete(element, right(t));
	else
		t->left = delete(element, left(t));
	return t;
}

Tree deleteMax(Tree t) {
	return delete(max(t), t);
}

Tree deleteRoot(Tree t) {
	Tree tmp = emptyTree();
	if (isEmptyTree(left(t))) {
		tmp = right(t);
		free(t);
		return tmp;
	}
	if (isEmptyTree(right(t))) {
		tmp = left(t);
		free(t);
		return tmp;
	}
	Element leftMax = max(left(t));
	deleteMax(left(t));
	t->element = leftMax;
	return t;
}
