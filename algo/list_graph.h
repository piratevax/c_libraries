#ifndef __LIST_GRAPH__
#define __LIST_GRAPH__

	#include <stdlib.h>
	#include <stdio.h>
	#include <stdbool.h>

	#define DIRECT 1
	#define REVERSE 2

	struct _List {
		float weight;
		int destination;
		struct _List * next;
	};
	typedef struct _List * List;
	//#include "graph.h"

	void setDestination(List l, int destination);
	void setWeight(List l, float weight);
	int getDestination(List l);
	float getWeight(List l);
	List getNext(List l);
	void setNext(List l, List next);
	List emptyList();
	int isEmptyList(List l);
	List addAtTheBegining (List l, float weight, int destination);
	int length(List l);
	void freeList (List l);
	/*void scan (List l, Graph g, int mode);
	void directScan(List l, Graph g);
	void reverseScan(List l, Graph g);*/

#endif
