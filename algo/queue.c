#include "queue.h"

Queue emptyQueue () {
	return NULL;
}

int isEmptyQueue (Queue q) {
	return q == NULL;
}

int getHead (Queue q) {
	return *((int*)getElement(q->head));
}

void enqueue (Queue q, int* element) {
	int flag = 0;
	if (isEmptyQueue(q)) {
		if ((q = (Queue) malloc(sizeof(struct _Queue))) == NULL) {
			fprintf(stderr, "Allocation error!\n");
			exit(1);
		}
		flag = 1;
	}
	q->tail = addAtTheEnd(q->tail, element);
	if (flag)
		q->head = q->tail;
}

void dequeue (Queue q) {
	List tmp = q->head;
	q->head = getNext(q->head);
	free(tmp);
}
