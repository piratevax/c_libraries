#include "list_graph.h"

void setDestination(List l, int destination) {
	l->destination = destination;
}

void setWeight(List l, float weight) {
	l->weight = weight;
}

int getDestination(List l) {
	return l->destination;
}

float getWeight(List l) {
	return l->weight;
}

List getNext(List l) {
	return l->next;
}

void setNext(List l, List next) {
	l->next = next;
}

List emptyList() {
	return NULL;
}

int isEmptyList(List l) {
	return l == NULL;
}

List addAtTheBegining (List l, float weight, int destination) {
	List p;
	if ((p = (List) malloc(sizeof(struct _List))) == NULL ) {
		fprintf(stderr, "Allocation error!");
		exit(1);
	}
	setWeight(p, weight);
	setDestination(p, destination);
	setNext(p, l);
	return p;
}

int length(List l) {
	if (isEmptyList(l))
		return 0;
	return 1 + length(getNext(l));
}

void freeList (List l) {
	if (isEmptyList(l))
		return;
	freeList(getNext(l));
	free(l);
}

/*void scan (List l, Graph g, int mode) {
	switch (mode) {
		case 1:
			//printf("Direct:\n");
			directScan(l, g);
			//printf("\n");
			break;
		case 2:
			//printf("Reverse:\n");
			reverseScan(l, g);
			//printf("\n");
			break;
		default:
			fprintf(stderr, "Display mode unknown!");
			break;
	}
}

void directScan(List l, Graph g) {
	if (isEmptyList(l))
		return;
	//printf("%d -> ", getElement(l));
	//printf("%d -> ", *((int*)getElement(l)));
	printf("\t%s: %f\n", getLabel(g, getDestination(l)), getWeight(l));
	directScan(getNext(l), g);
}

void reverseScan(List l, Graph g) {
	if (isEmptyList(l))
		return;
	reverseScan(getNext(l), g);
	//printf("%d -> ", getElement(l));
	//printf("%d -> ", *((int*)getElement(l)));
	printf("\t%s: %f\n", getLabel(g, getDestination(l)), getWeight(l));
}*/
