#ifndef __WORDS__
#define __WORDS__

	#include <stdlib.h>
	#include <stdio.h>
	#include <string.h>
	#include <time.h>

	#include "../c_courses/io.h"

	int naiveSearch (char*, int, char*, int);
	int* goodPref (char*, int);
	int morrisAndPrattSearch (char*, int, char*, int, int*);
	int* bestPref (char*, int);
	int knuthMorrisAndPrattSearch (char*, int, char*, int, int*);

#endif
