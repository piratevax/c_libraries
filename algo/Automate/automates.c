#include "automates.h"

ensemble creer_estTerminal(int nbE){
  ensemble e;
  if ((e = (ensemble) calloc(nbE, sizeof(int))) == NULL) {
	  fprintf(stderr, "ERROR: Can't allocate memory\n");
	  exit(EXIT_FAILURE);
  }

  return e;
}


void mettreTerminal(ensemble estT, etat e){
  estT[e] = TERM;
 }
 
ptransition* creer_transitions(int nbE){
  ptransition* tabT;
  
  if ((tabT = (ptransition *) malloc(nbE * sizeof(ptransition))) == NULL) {
	  fprintf(stderr, "ERROR: Can't allocate memory\n");
	  exit(EXIT_FAILURE);
  }
  for (int i = 0; i < nbE; i++)
	  tabT[i] = NULL;
  
  return tabT;
 }
 
 
void ajout_transition(ptransition* tabT, etat source, etat dest, char a){
  ptransition transition;
  ptransition sourceT = tabT[source];

  if ((transition = (ptransition) malloc(sizeof(struct transition))) == NULL) {
	  fprintf(stderr, "ERROR: Can't allocate memory\n");
	  exit(EXIT_FAILURE);
  }
  transition->lettre = a;
  transition->destination = dest;
  transition->suivant = NULL;
  
  while (sourceT != NULL)
	  sourceT = sourceT->suivant;
  sourceT->suivant = transition;
}

pautomate nouvel_automate(){
  pautomate A;
 
  if ((A = (pautomate) malloc(sizeof(struct automate))) == NULL) {
	  fprintf(stderr, "ERROR: Can't allocate memory\n");
	  exit(EXIT_FAILURE);
  }
 
 return A;
}

void init_automate(pautomate A, FILE *f){
	char *chaine;
	size_t taille;
	etat nbEtats = 0, tailleAlphabet = 0, etatInitial;
	ensemble estTerminal;
	ptransition* tabT;
	
	// On r�cup�re le nombre de bloc � lire pour lire le fichier d'un seul coup
	fseek(f, 0, SEEK_END);
	taille = ftell(f);
	fseek(f, 0, SEEK_SET);
	// Allocation de l'espace m�moire n�cesaire pour stocker le contenu du fichier
	if ((chaine = (char *) malloc((taille+1) * sizeof(char))) == NULL) {
		fprintf(stderr, "Can not allocate memory!\n");
		exit(EXIT_FAILURE);
	}
	// Lecture du fichier
	if (fread(chaine, taille, 1, f) != 1) {
		fprintf(stderr, "Can not read file!\n");//\"%s\"!\n", fichier);
		exit(EXIT_FAILURE);
	}
	chaine[taille] = '\0';
	for (int i = 0; i < taille; i++)
		if (chaine[i] == '\n')
			chaine[i] = '\0';

	int i=0, c = 0;
	char * lec = chaine;
	while (i < taille) {
		char a;
		etat etatT[3];
		etat src, dest;
		lec = lec + i;
		switch(c) {
			case 0:
				//nbEtats = atoi(chaine[i]);
				sscanf(lec,"%d", &nbEtats);
				estTerminal = creer_estTerminal(nbEtats);
				tabT = creer_transitions(nbEtats);
#ifdef DEBUG
				printf("%d: %d\n", c, nbEtats);
#endif
				c++;
				i += 2;
				break;
			case 1:
				//tailleAlphabet = atoi(chaine[i]);
				sscanf(lec,"%d", &tailleAlphabet);
#ifdef DEBUG
				printf("%d: %d\n", c, tailleAlphabet);
#endif
				c++;
				i =+ 2;
				break;
			case 2:
				//etatInitial = atoi(chaine[i]);
				sscanf(lec,"%d", &etatInitial);
#ifdef DEBUG
				printf("%d: %d\n", c, etatInitial);
#endif
				c++;
				i += 2;
				break;
			case 3:
				sscanf(lec,"%d %d %d", &etatT[0], &etatT[1], &etatT[2]);
#ifdef DEBUG
				printf("%d:", c);
#endif
				for (int j = 0; j < 3; j++) {
					mettreTerminal(estTerminal, etatT[j]);
#ifdef DEBUG
					printf(" %d", etatT[j]);
#endif
				}
				/*do {
					etat etatT = atoi(chaine[i]);
					mettreTerminal(estTerminal, etatT);
#ifdef DEBUG
				printf(" %d", etatT);
#endif
					while(chaine[++i] == ' ');
				}�while (chaine[i] != '\n');*/
#ifdef DEBUG
				printf("\n");
#endif
				c++;
				i += 6;
				break;
			default:
				sscanf(lec, "%d %c %d", &src, &a, &dest);
				/*src = atoi(chaine[i]);
				while(chaine[++i] == ' ');
				a = chaine[i];
				while(chaine[++i] == ' ');
				dest = atoi(chaine[i]);*/
#ifdef DEBUG
				printf("%d: %d %c %d\n", c, src, a, dest);
#endif
				ajout_transition(tabT, src, dest, a);
				c++;
				i += 6;
				break;
		}
	}
	A->nbetat = nbEtats;
	A->tailleAlpha = tailleAlphabet;
	A->etatInit = etatInitial;
	A->estTerminal = estTerminal;
	A->tabListeTrans = tabT;
}


pautomate creer_init_automate(char* fichier){
	FILE* df;
	pautomate A;
 
	A=nouvel_automate();
	if ((df = fopen(fichier, "r")) == NULL) {
		fprintf(stderr, "ERROR: Can not read \"%s\"!\n", fichier);
		exit(EXIT_FAILURE);
	}
	init_automate(A, df);
	fclose(df);
 
	return A;
}

void Affiche_automate(pautomate A){
    /* A compl�ter*/
  
}

etat destination (pautomate Aut, etat source, char a){
     /* A compl�ter*/
    return -1;
  
}

int estReconnu(pautomate Aut, char* mot){
   /* A compl�ter*/
  return 0;
  
}

