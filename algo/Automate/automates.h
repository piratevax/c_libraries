#ifndef __AUTOMATES__
#define __AUTOMATES__

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <string.h>

//#include "element.h"
//#include "../list.h"

#define TERM 1
#define INIT 0

//type etat
typedef int etat;

//tableau des �tats
typedef int* ensemble ;


//chaque etat a une lsite de transition

struct transition {
  char lettre;
  etat destination;
  struct transition *suivant; 
 };

typedef struct transition* ptransition;

// la structure de l'automate

typedef struct automate{
  int nbetat;
  int tailleAlpha;
  int etatInit;
  ensemble estTerminal;
  ptransition *tabListeTrans; 
  
}* pautomate;


ensemble creer_estTerminal(int nbE);

void mettreTerminal(ensemble estT, etat e);

ptransition* creer_transitions(int nbE);

void ajout_transition(ptransition* tabT, etat source, etat dest, char a);

pautomate nouvel_automate();

void init_automate(pautomate A, FILE *fauto);

pautomate creer_init_automate(char* nomF);

void Affiche_automate(pautomate A);

etat destination (pautomate Aut, etat source, char a);

int estReconnu(pautomate Aut, char* mot);

#endif
