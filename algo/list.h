#ifndef __LIST__
#define __LIST__

	#include <stdlib.h>
	#include <stdio.h>
	#include <stdbool.h>

	#define DIRECT 1
	#define REVERSE 2

	struct _List {
		//int element;
		void * element;
		struct _List * next;
	};
	typedef struct _List * List;

	//int getElement(List l);
	void* getElement(List l);
	//void setElement(List l, int element);
	void setElement(List l, void* element);
	List getNext(List l);
	void setNext(List l, List next);
	List emptyList();
	int isEmptyList(List l);
	//List addAtTheBegining (List l, int element);
	List addAtTheBegining (List l, void* element);
	void addAtTheEnd (List l, void* element);
	void scan (List l, int mode);
	void directScan(List l);
	int length(List l);
	void reverseScan(List l);
	void freeList (List l);

#endif
