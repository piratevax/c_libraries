#ifndef __GRAPH__
#define __GRAPH__

	#include <stdlib.h>
	#include <stdio.h>
	#include <stdbool.h>

	#include "list_graph.h"	

	#define TO_SEE 0
	#define SEEN 1

	struct _Vertex {
		char *label;
		List heirs;
	};
	typedef struct _Vertex *Vertex;

	struct _Graph {
		int vertexNumber;
		Vertex *v;
	};
	typedef struct _Graph *Graph;

	int isEmptyGraph(Graph);
	// Create a n-vertices graph
	Graph newGraph(int);
	Vertex newVertex(char*);
	int getNumberOfVertices(Graph);
	// Getting the k-th vertex
	Vertex getVertex(Graph, int);
	List getHeirs(Vertex);
	void newEdge(Graph, int, float,int);
	void setLabel(Graph, int, char*);
	char* getLabel(Graph, int);
	void scanGraph(Graph);
	void scanList(List l, Graph g, int mode);
	void directScan(List l, Graph g);
	void reverseScan(List l, Graph g);

	void visiteEnProfondeur(Graph, int, int*);
	void parcoursEnProfondeur(Graph);

#endif
