#ifndef __TREE__
#define __TREE__

	#include <stdlib.h>
	#include <stdio.h>
	
	typedef int Element;
	struct _Tree {
		Element element;
		struct _Tree * left;
		struct _Tree * right;
	};
	typedef struct _Tree * Tree;

	Tree emptyTree();
	int isEmptyTree(Tree t);
	Element root(Tree t);
	Tree left(Tree t);
	Tree right(Tree t);
	Tree build(Element element, Tree left, Tree right);
	void prefixProcess (Tree t);
	void infixProcess(Tree t);
	void suffixProcess(Tree t);
	void process(Tree t, int mode);
	void printTree(Tree t, int level);
	Tree insert(Element element, Tree t);
	int search(Element element, Tree t);
	Element max(Tree t);
	Tree delete(Element element, Tree t);
	Tree deleteMax(Tree t);
	Tree deleteRoot(Tree t);

#endif
