#ifndef __PILE__
#define __PILE__

	#include <stdlib.h>

	#include "list.h"

	List emptyStack();
	int isEmptyStack (List l);
	int getTop(List l);
	List push (List l, int* element);
	List pop (List l);

#endif
