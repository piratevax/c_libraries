#ifndef __QUEUE__
#define __QUEUE__

	#include <stdlib.h>

	#include "list.h"

	struct _Queue {
		List head;
		List tail;
	};
	typedef struct _Queue* Queue; 

	Queue emptyQueue ();
	int isEmptyQueue (Queue q);
	int getHead(Queue q);
	void enqueue (Queue q, int* element);
	void dequeue (Queue q);

#endif

