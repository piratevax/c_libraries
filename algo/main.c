#include "main.h"

int main (int argc, char** argv) {
// Testing list
	/*List l = NULL;
	int element[] = {9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
	for (int i = 0; i < DEF; i++)
		l = addAtTheBegining(l, &element[i]);

	printf("Length: %d\n", length(l));
	scan(l, DIRECT);
	l = pop(l);
	scan(l, REVERSE);

	freeList(l);*/

// Testing tree
	//Element element[] = {5, 8, 3, 6, 9, 4, 7, 2, 0, 1};
	//Tree t = emptyTree();
	/*Tree t = build(element[0],
			build(element[1],
				build(element[3],
					build(element[7],
						emptyTree(),
						emptyTree()),
					build(element[8],
						emptyTree(),
						emptyTree())),
				build(element[4],
					build(element[9],
						emptyTree(),
						emptyTree()),
					emptyTree())),
			build(element[2],
				build(element[5],
						emptyTree(),
						emptyTree()),
				build(element[6],
					emptyTree(),
					emptyTree()))
		);*/
	/*for (int i = 0; i < DEF; i++) {
		t = insert(element[i], t);
	}
	printTree(t, 0);
	process(t, PRE);
	process(t, INF);
	process(t, SUF);
	printf("7 => %d\n10 => %d\n", search(7, t), search(10, t)); 
	t = deleteRoot(t);
	printTree(t, 0);
	process(t, 2);*/

// Testing graph
	int n = 12, def = 0;
	char labels[12][2] = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"};

	Graph g = newGraph(n);
	/*setLabel(g, 0, "A\0");//labels[0]);
	newEdge(g, 0, 1.4, 1);
	newEdge(g, 0, 2.3, 2);
	setLabel(g, 1, "B\0");//labels[1]);
	newEdge(g, 1, 2.0, 1);
	newEdge(g, 1, 7.0, 2);
	setLabel(g, 2, "C\0");//labels[2]);
	newEdge(g, 2, 6.0, 0);*/
	for (int i = 0; i < n; i++)
		setLabel(g, i, labels[i]);
	newEdge(g, 0, def, 3);
	newEdge(g, 0, def, 2);
	newEdge(g, 0, def, 1);
	newEdge(g, 1, def, 5);
	newEdge(g, 1, def, 4);
	newEdge(g, 2, def, 8);
	newEdge(g, 2, def, 7);
	newEdge(g, 2, def, 6);
	newEdge(g, 3, def, 5);
	newEdge(g, 3, def, 1);
	newEdge(g, 4, def, 2);
	newEdge(g, 4, def, 0);
	newEdge(g, 5, def, 8);
	newEdge(g, 5, def, 6);
	newEdge(g, 6, def, 4);
	newEdge(g, 7, def, 6);
	newEdge(g, 7, def, 5);
	newEdge(g, 8, def, 3);
	newEdge(g, 9, def, 11);
	newEdge(g, 9, def, 10);
	newEdge(g, 11, def, 10);
#ifdef DEBUG
	scanGraph(g);
#endif
	parcoursEnProfondeur(g);
	exit(EXIT_SUCCESS);
}
