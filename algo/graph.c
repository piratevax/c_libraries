#include "graph.h"

int isEmptyGraph(Graph g) {
	if (g == NULL)
		return 0;
	return 1;
}

// Create a n-vertices graph
Graph newGraph(int vertexNumber) {
	Graph g = NULL;
	Vertex *v = NULL;
	if ((g = (Graph) malloc(sizeof(struct _Graph))) == NULL) {
		fprintf(stderr, "Graph allocation error!\n");
		exit(1);
	}
	g->vertexNumber = vertexNumber;
	if ((v = (Vertex *) malloc(vertexNumber * sizeof(Vertex))) == NULL) {
		fprintf(stderr, "Verteces allocation error!\n");
		exit(1);
	}
	for (int i = 0; i < vertexNumber; i++)
		v[i] = newVertex("");
	g->v = v;
	return g;
}

Vertex newVertex(char *label) {
	Vertex v = NULL;
	if ((v = (Vertex) malloc(sizeof(struct _Vertex))) == NULL) {
		fprintf(stderr, "Vertex allocation error!\n");
		exit(1);
	}
	v->label = label;
	v->heirs = emptyList();
	return v;
}

int getNumberOfVertices(Graph g) {
	return g->vertexNumber;
}

// Getting the k-th vertex
Vertex getVertex(Graph g, int k) {
	return g->v[k];
}

List getHeirs(Vertex v) {
	return v->heirs;
}

// ajoute un arc (s,v,d) au graphe non vide g
void newEdge(Graph g, int s, float v, int d) {
	List l = g->v[s]->heirs;
	g->v[s]->heirs = addAtTheBegining(l, v, d);
}

void setLabel(Graph g, int k, char* label) {
	g->v[k]->label = label;
}

char* getLabel(Graph g, int k) {
	return g->v[k]->label;
}

void scanGraph(Graph g) {
	int n = getNumberOfVertices(g);

	for (int i = 0; i < n; i++) {
		printf("%s\n", getLabel(g, i));
		scanList(getHeirs(getVertex(g, i)), g, 1);
	}
}

void scanList(List l, Graph g, int mode) {
	switch (mode) {
		case 1:
			//printf("Direct:\n");
			directScan(l, g);
			//printf("\n");
			break;
		case 2:
			//printf("Reverse:\n");
			reverseScan(l, g);
			//printf("\n");
			break;
		default:
			fprintf(stderr, "Display mode unknown!");
			break;
	}
}

void directScan(List l, Graph g) {
	if (isEmptyList(l))
		return;
	//printf("%d -> ", getElement(l));
	//printf("%d -> ", *((int*)getElement(l)));
	printf("\t%s: %.2f\n", getLabel(g, getDestination(l)), getWeight(l));
	//printf("\t%f\n", getWeight(l));
	directScan(getNext(l), g);
}

void reverseScan(List l, Graph g) {
	if (isEmptyList(l))
		return;
	reverseScan(getNext(l), g);
	//printf("%d -> ", getElement(l));
	//printf("%d -> ", *((int*)getElement(l)));
	printf("\t%s: %.2f\n", getLabel(g, getDestination(l)), getWeight(l));
	//printf("\t%f\n", getWeight(l));
}

void visiteEnProfondeur(Graph g, int v, int* flag) {
	List l = getHeirs(getVertex(g, v));
	if (isEmptyList(l))
		return;
	flag[v] = SEEN;
	printf("%s\n", getLabel(g, v));
	//for (int i = 0; i < getNumberOfVertices(g); i++)
	do {
		int dest = getDestination(l);
		if (! flag[dest])
			visiteEnProfondeur(g, dest, flag);
	} while ((l = getNext(l)) != NULL);
}

void parcoursEnProfondeur(Graph g) {
	int numberOfVertices = getNumberOfVertices(g), i;
	int *flag;
	if ((flag = (int *) malloc(numberOfVertices * sizeof(int))) == NULL) {
		fprintf(stderr, "Can't alocate memory!\n");
		exit(1);
	}
	for (i = 0; i < numberOfVertices; i++)
		flag[i] = TO_SEE;
	for (i = 0; i < numberOfVertices; i++) {
#ifdef DEBUG
		printf("*D*| i: %d\n", i);
#endif
		if (! flag[i])
			visiteEnProfondeur(g, i, flag);
	}
	free(flag);
}
