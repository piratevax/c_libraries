#include "words.h"

int naiveSearch (char* word, int wordL, char* text, int textL) {
	int occurence = 0;

	for (int i = 0; i < (textL - wordL); i++) {
		/*int count = 0;
		for	(int j = 0; j < wordL; j++) {
			if (word[j] == text[i+j])
				count++;
		}
		if (count == wordL)
			occurence++;*/
		if (!strncmp(word, text+i, wordL))
			occurence++;
	}
	return occurence;	
}

int * goodPref(char * word, int wordL) {
	int * pref = NULL;
	if ((pref = (int *) malloc((wordL + 1) * sizeof(int))) == NULL) {
		fprintf(stderr, "Can't allocate memory!\n");
		exit(1);
	}
	int i = 0;
	pref[i] = -1;
	for (int j = 1; j < wordL; j++) {
		pref[j] = i;
		while (i >= 0 && word[i] != word[j])
			i = pref[i];
		i++;
	}
	pref[wordL] = i;
#ifdef DEBUG
	printf("*D*| prefArray: [%d", pref[0]);
	for (int i = 1; i <= wordL; i++)
		printf(",%d", pref[i]);
	printf("]\n");
#endif
	return pref;
}

int morrisAndPrattSearch (char* word, int wordL, char* text, int textL, int* pref) {
	int i = 0, occurence = 0;
	for (int j = 0; j < textL; j++) {
		while (i >= 0 && word[i] != text[j])
			i = pref[i];
		i++;
		if (i == wordL) {
			occurence++;
			i = pref[i];
		}
	}
	return occurence;
}

int* bestPref (char* word, int wordL) {
	int * pref = NULL;
	if ((pref = (int *) malloc((wordL + 1) * sizeof(int))) == NULL) {
		fprintf(stderr, "Can't allocate memory!\n");
		exit(1);
	}
	int i = 0;
	pref[i] = -1;
	for (int j = 1; j < wordL; j++) {
		if (word[i] == word[j])
			pref[j] = pref[i];
		else {
			pref[j] = i;
			do {
				i = pref[i];
			} while (i >= 0 && word[i] != word[j]);
		}
		i++;
	}
	pref[wordL] = i;
#ifdef DEBUG
	printf("*D*| bestPrefArray: [%d", pref[0]);
	for (int i = 1; i <= wordL; i++)
		printf(",%d", pref[i]);
	printf("]\n");
#endif
	return pref;
}

int knuthMorrisAndPrattSearch (char* word, int wordL, char* text, int textL, int* pref) {
	int i = 0, occurence = 0;

	for (int j = 0; j < textL; j++) {
		while (i >= 0 && word[i] != text[j])
			i = pref[i];
		i++;
		if (i == wordL) {
			occurence++;
			i = pref[i];
		}
	}
	return occurence;
}

int main (int argc, char** argv) {
	if (argc < 3) {
		fprintf(stderr, "No arguments!\n");
		exit(1);
	}
	char * word = argv[1];
	char * text = readFile(argv[2]);
	clock_t timeM;
	
	srand(time(NULL));
	printf("Naive search:\n\t'%s' found %d times.\n", word, naiveSearch(word, strlen(word), text, strlen(text)));
	timeM = clock();
	printf("\ttime: %lfs\n\n", (double) timeM/CLOCKS_PER_SEC);//(before-after)/3600.0);
	int * goodPrefArray = goodPref(word, strlen(word));
	srand(time(NULL));
	printf("Morris and Pratt search:\n\t'%s' found %d times.\n", word, morrisAndPrattSearch(word, strlen(word), text, strlen(text), goodPrefArray));
	timeM = clock();
	printf("\ttime: %lfs\n\n", (double) timeM/CLOCKS_PER_SEC);//(before-after)/3600.0);
	int * bestPrefArray = bestPref(word, strlen(word));
	srand(time(NULL));
	printf("Knuth, Morris and Pratt search:\n\t'%s' found %d times.\n", word, morrisAndPrattSearch(word, strlen(word), text, strlen(text), bestPrefArray));
	timeM = clock();
	printf("\ttime: %lfs\n\n", (double) timeM/CLOCKS_PER_SEC);//(before-after)/3600.0);
	//printf("Knuth, Morris and Pratt search:\n\t'%s' found %d times.\n\n", word, knuthMorrisAndPrattSearch(word, strlen(word), text, strlen(text), bestPrefArray));

	free(text);
	free(goodPrefArray);
	free(bestPrefArray);

	return 0;
}
