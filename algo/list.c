#include "list.h"

/*int getElement(List l) {
	return l->element;
}*/
void* getElement(List l) {
	return l->element;
}

/*void setElement(List l, int element) {
	l->element = element;
}*/
void setElement(List l, void* element) {
	l->element = element;
}

List getNext(List l) {
	return l->next;
}

void setNext(List l, List next) {
	l->next = next;
}

List emptyList() {
	return NULL;
}

int isEmptyList(List l) {
	return l == NULL;
}

/*List addAtTheBegining (List l, int element) {
	List p;
	if ((p = (List) malloc(sizeof(struct _List))) == NULL ) {
		fprintf(stderr, "Allocation error!");
		exit(1);
	}
	setElement(p, element);
	setNext(p, l);
	return p;
}*/
List addAtTheBegining (List l, void* element) {
	List p;
	if ((p = (List) malloc(sizeof(struct _List))) == NULL ) {
		fprintf(stderr, "Allocation error!");
		exit(1);
	}
	setElement(p, element);
	setNext(p, l);
	return p;
}

/*lA : source
lB : destination
*/
List addAtTheBeginingList (List lA, List lB) {
	setNext(lB, lA);
	return lB;
}

void addAtTheEnd (List l, void* element) {
	List p;
	if ((p = (List) malloc(sizeof(struct _List))) == NULL ) {
		fprintf(stderr, "Allocation error!");
		exit(1);
	}
	setElement(p, element);
	setNext(l, p);
}

void scan (List l, int mode) {
	switch (mode) {
		case 1:
			printf("Direct:\n");
			directScan(l);
			printf("\n");
			break;
		case 2:
			printf("Reverse:\n");
			reverseScan(l);
			printf("\n");
			break;
		default:
			fprintf(stderr, "Display mode unknown!");
			break;
	}
}

void directScan(List l) {
	if (isEmptyList(l))
		return;
	//printf("%d -> ", getElement(l));
	printf("%d -> ", *((int*)getElement(l)));
	directScan(getNext(l));
}

int length(List l) {
	if (isEmptyList(l))
		return 0;
	return 1 + length(getNext(l));
}

void reverseScan(List l) {
	if (isEmptyList(l))
		return;
	reverseScan(getNext(l));
	//printf("%d -> ", getElement(l));
	printf("%d -> ", *((int*)getElement(l)));
}

void freeList (List l) {
	if (isEmptyList(l))
		return;
	freeList(getNext(l));
	free(l);
}

List allocate (void* element) {
	List l = NULL;
	if ((l = (List) malloc(sizeof(struct _List))) == NULL) {
		fprintf(stderr, "Allocation error!");
		exit(1);
	}
	setElement(l, element);
	return l;
}


/*int allocate(List *array, int available) {
	int tmp = 0;
	if (!available)
		return available;
	tmp = available;
	available = getNext(array[available]);
	return tmp;
}*/

List insertInOrderedList (List head, void * element) {
	if (isEmptyList(head)) {
		return addAtTheBegining(head, element);
	}
	int a = *((int*) getElement(head)), b = *((int*) element);

	if( a > b) {
		return addAtTheBegining(head, element);
	}
	setNext(head, insertInOrderedList(getNext(head), element));
	return head;
	// return addAtTheBegining(head, insertInOrderedList(head, element));
}

List duplicateList (List l) {
	List copy = emptyList();
	if (isEmptyList(l))
		return copy;
	copy = allocate(getElement(l));
	setNext(copy, duplicateList(getNext(l)));
	return copy;
}

List mergeOrderedLists (List listA, List listB) {
	List list = emptyList();
	if (isEmptyList(listA) && isEmptyList(listB))
		return emptyList();
	if (isEmptyList(listA))
		return duplicateList(listB);
	if (isEmptyList(listB))
		return duplicateList(listA);
	if (*((int*) getElement(listA)) > *((int*) getElement(listB))) {
		list = listB;
		setNext(list, mergeOrderedLists(listA, getNext(listB)));
	} else {
		list = listA;
		setNext(list, mergeOrderedLists(getNext(listA), listB));
	}
	return list;
}

int main (int argc, char **argv) {
	List head = emptyList(), headB = emptyList();
	int tab[] = {1, 7, 9, 12, 8};
	//           0  1  2   3  4
	int tab2[] = {3, 5, 12};
	for (int i = 3; i > -1; i--)
		head = addAtTheBegining(head, &tab[i]);
	for (int i = 2; i > -1; i--)
		headB = addAtTheBegining(headB, &tab2[i]);
	scan(head, 1);
	insertInOrderedList(head, &tab[4]);
	scan(head, 1);
	scan(headB, 1);

	List newHead = mergeOrderedLists(head, headB);
	scan(newHead, 1);
}
